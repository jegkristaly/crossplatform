const DemoModel = require("./demo-model");
const frameModule = require("ui/frame");

exports.navigatingTo = function(e) {
    const page = e.object;
    page.bindingContext = new DemoModel();
};

exports.onNavBtnTap = function onNavBtnTap(eventData) {
    frameModule.topmost().goBack();
};

exports.sbLoaded = function sbLoaded(eventData){
    eventData.object.android.clearFocus();
};

exports.onChildTap = function onChildTap(eventData) {
    const navigationOptions = {
        moduleName: 'views/measurement/sensors/sensor-details/sensor-details-page',
        context: {
            sensorId: eventData.object.sensorId,
            from: "demo"
        }
    };
    frameModule.topmost().navigate(navigationOptions);
};

exports.onNewSensorTap = function onNewSensorTap(eventData){
    const navigationOptions = {
        moduleName: 'views/measurement/sensors/sensor-add/sensor-add-page',
    };
    frameModule.topmost().navigate(navigationOptions);
};

exports.onNotImplemented = function onNotImplemented(){
    alert("not implemented");
};