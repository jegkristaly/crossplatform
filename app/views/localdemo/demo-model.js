var Observable = require("data/observable").Observable;
var _ = require("lodash");

module.exports = function() {
    var model = new Observable();

    model.allItems = [];
    model.filter   = "";
    model.items    = [];
    model.sort_mode = 'desc';
    model.type_sort = "";
    model.isLoading = true;
    reloadFromBackend();

    model.onSortTap = function(eventData){
        model.set("items", _sort());
        switch(parseInt(eventData.object.tapCounter)){
            case 0:
                model.sort_mode="asc";
                eventData.object.tapCounter = "1";
                eventData.object.text = String.fromCharCode(parseInt('f15e',16));
                break;
            case 1:
                model.sort_mode="desc";
                eventData.object.text = String.fromCharCode(parseInt('f15d',16));
                eventData.object.tapCounter = "0";
                break;
        }
    };

    model.onFilterTap = function(eventData){
        if(eventData.object.active === false) {
            model.type_sort = eventData.object.type;
        } else {
            model.type_sort = "";
        }
        model.set("items", _refilterOnType(model.type_sort));
    };

    model.onPullToRefreshInitiated = function (eventData) {
        reloadFromBackend();
        eventData.object.notifyPullToRefreshFinished();
    };

    function _refilter() {
        var items = model.allItems;
        var filter = model.filter;
        if (filter) {
            var regexp = new RegExp(filter, 'i');
            items = _.filter(items, function(i) {
                return i.name.match(regexp);
            });
        }
        return items;
    }

    function _refilterOnType(eventData){
        var items = model.allItems;
        var regexp = new RegExp(eventData, 'i');
        items = _.filter(items, function(i) {
            return i.real_type.match(regexp);
        });
        return items;
    }

    function _sort(){
        var items = model.items;
        items = _.orderBy(items, ['name'], [model.sort_mode]);
        return items;
    }

    function reloadFromBackend(){
        model.allItems = [];
        fetch(global.backendUrl + '/sensors', {
                headers: {
                    'Authorization': 'Bearer ' + global.accessToken
                }
            }
        ).then(response => { return response.json(); }).then(function (r) {
            for(var i = 0; i < r.length; i++){
                var _type;
                var jsonstatus = r[i].type;
                switch(jsonstatus){
                    case "temperature":
                        _type = String.fromCharCode(parseInt('f2c8',16));
                        break;
                    case "humidity":
                        _type = String.fromCharCode(parseInt('f043',16));
                        break;
                    case "noise":
                        _type = String.fromCharCode(parseInt('f028',16));
                        break;
                    case "light":
                        _type = String.fromCharCode(parseInt('f0eb',16));
                        break;
                }
                model.allItems.push({id: r[i]._id, name: r[i].name, type: _type, real_type: r[i].type});
            }
        }).then(function(){
            model.set("items", _refilter());
        }).then(function(){
            model.set("isLoading", false);
        }).catch(function(error) {
            alert("It seems like you are not connected to the internet.")
        });
    }

    model.on(Observable.propertyChangeEvent, function(e) {
        if (e.propertyName === "filter") model.set("items", _refilter());
    });

    return model;
};