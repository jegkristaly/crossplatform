const frameModule = require("ui/frame");
let emailTextField;
let passwordTextField;
const Toast = require("nativescript-toast");
const Observable = require("data/observable").Observable;

exports.onNavigatingTo = function onNavigatingTo(args) {
    let page = args.object;
    emailTextField = page.getViewById("emailTextField");
    passwordTextField = page.getViewById("passwordTextField");
    page.bindingContext = new Observable();
};

exports.onTap = function onTap(eventData) {
    if(emailTextField.text && passwordTextField.text) {
        fetch(global.backendUrl + '/login', {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
                username: emailTextField.text,
                password: passwordTextField.text
            })
        }).then(function(response) {
            if(response.ok) {
                return response.json();
            } else {
                response.json().then(json => {Toast.makeText(json.message, "long").show();});
                throw new Error(); //enélkül futna a következő promise
            }
        }).then(function(json){
            global.accessToken = json.token;
            const navigationOptions = {
                moduleName: 'views/landing/landing-page',
                context: {
                    creator: emailTextField.text
                }
            };
            frameModule.topmost().navigate(navigationOptions);
        }).catch(function(error) {
            //nem csinálunk semmit
        });
    } else {
        Toast.makeText("All Fields Required!", "long").show();
    }
};

exports.onRegisterTap = function onRegisterTap(){
    const navigationOptions = {
        moduleName: 'views/register/register-page',
    };
    frameModule.topmost().navigate(navigationOptions);
};