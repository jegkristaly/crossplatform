const frameModule = require("ui/frame");
let creator;
const BarcodeScanner = require("nativescript-barcodescanner").BarcodeScanner;
const barcodeScanner = new BarcodeScanner();
const Observable = require("data/observable").Observable;

exports.onNavigatingTo = function onNavigatingTo(args) {
    let page = args.object;
    const gotData = page.navigationContext;
    creator = gotData.creator;
    page.bindingContext = new Observable();
};

exports.onMeasurementsTap = function onMeasurementsTap(eventData){
    const navigationOptions = {
        moduleName: 'views/measurement/measurements-page',
        context: {
            creator: creator
        }
    };
    frameModule.topmost().navigate(navigationOptions);
};

exports.onSensorsTap = function onSensorsTap(eventData){
    const navigationOptions = {
        moduleName: 'views/sensor/sensor-page',
        context: {
            creator: creator
        }
    };
    frameModule.topmost().navigate(navigationOptions);
};

exports.onDemoTap = function onDemoTap(eventData){
    const navigationOptions = {
        moduleName: 'views/localdemo/demo',
        context: {
            creator: creator
        }
    };
    frameModule.topmost().navigate(navigationOptions);
};

exports.onNotImplemented = function onNotImplemented(){
    alert("not implemented");
};

exports.onQRRreaderTap = function onQRRead(){
    barcodeScanner.scan({
        formats: "QR_CODE",   // Pass in of you want to restrict scanning to certain types
        cancelLabel: "EXIT. Also, try the volume buttons!", // iOS only, default 'Close'
        cancelLabelBackgroundColor: "#333333", // iOS only, default '#000000' (black)
        message: "Use the volume buttons for extra light", // Android only, default is 'Place a barcode inside the viewfinder rectangle to scan it.'
        showFlipCameraButton: false,   // default false
        preferFrontCamera: false,     // default false
        showTorchButton: true,        // default false
        beepOnScan: true,             // Play or Suppress beep on scan (default true)
        torchOn: false,               // launch with the flashlight on (default false)
        closeCallback: function () { console.log("Scanner closed"); }, // invoked when the scanner was closed (success or abort)
        resultDisplayDuration: 1500,   // Android only, default 1500 (ms), set to 0 to disable echoing the scanned text
        orientation: "landscape",     // Android only, optionally lock the orientation to either "portrait" or "landscape"
        openSettingsIfPermissionWasPreviouslyDenied: true // On iOS you can send the user to the settings app if access was previously denied
    }).then(
        function(result) {
            const navigationOptions = {
                moduleName: 'views/measurement/sensors/sensor-details/sensor-details-page',
                context: {
                    sensorId: result.text,
                    from: "demo"
                }
            };
            frameModule.topmost().navigate(navigationOptions);
        },
        function(error) {
            alert("No scan: " + error);
        }
    );
};