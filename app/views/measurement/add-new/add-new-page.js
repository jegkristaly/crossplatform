var frameModule = require("ui/frame");
var creator;
var nameTextField;
var durationTextField;
var timePicker;
var datePicker;
var Toast = require("nativescript-toast");
const Observable = require("data/observable").Observable;

exports.onNavigatingTo = function onNavigatingTo(args) {
    var page = args.object;
    var gotData=page.navigationContext;
    creator = gotData.creator;
    nameTextField = page.getViewById("nameTextField");
    durationTextField = page.getViewById("durationTextField");
    timePicker = page.getViewById("timePicker");
    datePicker = page.getViewById("datePicker");
    page.bindingContext = new Observable();
};

exports.onNavBtnTap = function onNavBtnTap(eventData) {
    frameModule.topmost().goBack();
};

exports.onCreateTap = function onCreateTap(eventData) {
    var date = new Date(
        datePicker.year,
        datePicker.month - 1,
        datePicker.day,
        timePicker.hour,
        timePicker.minute
    );

    fetch(global.backendUrl + '/measurements', {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            'Authorization': 'Bearer ' + global.accessToken
        },
        body: JSON.stringify({
            creator: creator,
            name: nameTextField.text,
            duration: durationTextField.text,
            startDate: date
        })
    }).then(r => { return r.json(); }).then(function (r) {
        Toast.makeText("Measurement created", "long").show();
    }).catch(function(error) {
        console.log(error);
    });
    frameModule.topmost().goBack();
};