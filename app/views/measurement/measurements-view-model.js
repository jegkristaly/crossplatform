var Observable = require("data/observable").Observable;
var _ = require("lodash");

module.exports = function() {
    var model = new Observable();

    model.allItems = [];
    model.filter   = "";
    model.items    = [];
    model.sort_mode = 'desc';
    model.status_sort = "";
    reloadFromBackend();

    model.onSortTap = function(eventData){
        model.set("items", _sort());
        switch(parseInt(eventData.object.tapCounter)){
            case 0:
                model.sort_mode="asc";
                eventData.object.tapCounter = "1";
                eventData.object.text = String.fromCharCode(parseInt('f15e',16));
                break;
            case 1:
                model.sort_mode="desc";
                eventData.object.text = String.fromCharCode(parseInt('f15d',16));
                eventData.object.tapCounter = "0";
                break;
        }
    };

    model.onFilterTap = function(eventData){
        if(eventData.object.active === false) {
            model.status_sort = eventData.object.status;
        } else {
            model.status_sort = "";
        }
        model.set("items", _refilterOnStatus(model.status_sort));
    };

    model.onPullToRefreshInitiated = function (eventData) {
        reloadFromBackend();
        eventData.object.notifyPullToRefreshFinished();
    };

    function _refilter() {
        var items = model.allItems;
        var filter = model.filter;
        if (filter) {
            var regexp = new RegExp(filter, 'i');
            items = _.filter(items, function(i) {
                return i.name.match(regexp);
            });
        }
        return items;
    }

    function _sort(){
        var items = model.items;
        items = _.orderBy(items, ['name'], [model.sort_mode]);
        return items;
    }

    function _refilterOnStatus(eventData){
        var items = model.allItems;
        var regexp = new RegExp(eventData, 'i');
        items = _.filter(items, function(i) {
            return i.real_status.match(regexp);
        });
        return items;
    }

    function reloadFromBackend(){
        model.allItems = [];
        fetch(global.backendUrl + '/measurements', {
            headers: {
                'Authorization': 'Bearer ' + global.accessToken
            }
        }).then(response => { return response.json(); }).then(function (r) {
            for(var i = 0; i < r.length; i++){
                var status_string;
                var status_color_string;
                var jsonstatus = JSON.stringify(r[i].live);
                if(jsonstatus=="false"){
                    status_string = String.fromCharCode(parseInt('f28d',16));
                    status_color_string = "list-red";
                } else {
                    status_string = String.fromCharCode(parseInt('f111',16));
                    status_color_string = "list-green";
                }
                model.allItems.push({name: r[i].name, status: status_string,
                    selected_class: "fa list-icon-label " + status_color_string, recordId: r[i]._id,
                    real_status: JSON.stringify(r[i].live)});
            }
        }).then(function(){
            model.set("items", _refilter());
        }).catch(function(error) {
            console.log(error);
        });
    }

    model.on(Observable.propertyChangeEvent, function(e) {
        if (e.propertyName === "filter") model.set("items", _refilter());
    });

    return model;
};