var Observable = require("data/observable").Observable;
var frameModule = require("ui/frame");
var measurementId;

exports.onNavigatingTo = function onNavigatingTo(args) {
    var page = args.object;
    var gotData=page.navigationContext;
    measurementId = gotData.measurementId;

    fetch(global.backendUrl + '/measurements/' + measurementId, {
        headers: {
            'Authorization': 'Bearer ' + global.accessToken
        }
    }).then(response => { return response.json(); }).then(function (r) {
        page.bindingContext = ({
            status: r.live,
            name: r.name,
            creator: r.creator,
            startDate: r.startDate,
            duration: r.duration
        });
    }).catch(function(error) {
        console.log(error);
    });

    page.bindingContext = new Observable();
};

exports.onNavBtnTap = function onNavBtnTap(eventData) {
    frameModule.topmost().goBack();
};