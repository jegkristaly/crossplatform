var SensorsViewModel = require("./sensors-view-model");
var frameModule = require("ui/frame");
var measurementId;

exports.navigatingTo = function(e) {
    var page = e.object;
    var gotData=page.navigationContext;
    measurementId = gotData.measurementId;
    page.bindingContext = new SensorsViewModel(measurementId);
};

exports.onNavBtnTap = function onNavBtnTap(eventData) {
    frameModule.topmost().goBack();
};

exports.sbLoaded = function sbLoaded(eventData){
    eventData.object.android.clearFocus();
};

exports.onChildTap = function onChildTap(eventData) {
    var navigationOptions={
        moduleName:'views/measurement/sensors/sensor-details/sensor-details-page',
        context:{
            sensorId: eventData.object.sensorId,
            measurementId: measurementId,
            from: "measurements"
        }
    };
    frameModule.topmost().navigate(navigationOptions);
};

exports.onNewSensorTap = function onNewSensorTap(eventData){
    var navigationOptions={
        moduleName:'views/measurement/sensors/sensor-add/sensor-add-page',
    };
    frameModule.topmost().navigate(navigationOptions);
};

exports.onNotImplemented = function onNotImplemented(){
    alert("not implemented");
};



/*var frameModule = require("ui/frame");
var SensorsViewModel = require("./sensors-view-model");
var observable = require("data/observable");
var sensorsViewModel = new SensorsViewModel();
var viewModel = new observable.Observable();
var measurementId;
var observableArray = require("data/observable-array");
var ids = [];
function onNavigatingTo(args) {
    var page = args.object;
    var gotData=page.navigationContext;
    measurementId = gotData.measurementId;
    var array = new observableArray.ObservableArray();
    ids=[];

    fetch(global.backendUrl + '/measurements/' + measurementId).then(response => { return response.json(); }).then(function (r) {
        for(var i = 0; i < r.sensors.length; i++){
            fetch(global.backendUrl + '/sensors/' + r.sensors[i]).then(response => { return response.json(); }).then(function (r) {
                array.push({id: r._id, name: r.name, type_and_distribution: r.type + " - " + r.distribution });
                ids.push(r._id);
            }).catch(function(error) {
                console.log(error);
            });
        }
        page.bindingContext = {items: array};
    }).catch(function(error) {
        console.log(error);
    });
}

exports.onNavigatingTo = onNavigatingTo;

function onChildTap(eventData) {
    var navigationOptions={
        moduleName:'views/measurement/sensors/sensor-details/sensor-details-page',
        context:{sensorId: eventData.object.sensorId
        }
    };
    frameModule.topmost().navigate(navigationOptions);
}
exports.onChildTap = onChildTap;

function onNewSensorTap(eventData){
    var navigationOptions={
        moduleName:'views/measurement/sensors/sensor-add/sensor-add-page',
        context:{measurementId: measurementId, measurementSensorIds: ids.slice()
        }
    };
    ids = [];
    frameModule.topmost().navigate(navigationOptions);
}
exports.onNewSensorTap = onNewSensorTap;

function onNavBtnTap(eventData) {
    frameModule.topmost().goBack();
}
exports.onNavBtnTap = onNavBtnTap;

function onRefresh(eventData) {
    alert("Not implemented!");
}
exports.onRefresh = onRefresh;*/