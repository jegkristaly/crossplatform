var Observable = require("data/observable").Observable;
var _ = require("lodash");
var Toast = require("nativescript-toast");

module.exports = function(measurementId) {
    var model = new Observable();
    var mId = measurementId;

    var addedSensorIds = [];

    model.allItems = [];
    model.filter   = "";
    model.items    = [];
    model.sort_mode = 'desc';
    model.type_sort = "";
    model.added_filter = "";
    model.added_class="button-secondary fa";
    reloadFromBackend();

    model.onSortTap = function(eventData){
        model.set("items", _sort());
        switch(parseInt(eventData.object.tapCounter)){
            case 0:
                model.sort_mode="asc";
                eventData.object.tapCounter = "1";
                eventData.object.text = String.fromCharCode(parseInt('f15e',16));
                break;
            case 1:
                model.sort_mode="desc";
                eventData.object.text = String.fromCharCode(parseInt('f15d',16));
                eventData.object.tapCounter = "0";
                break;
        }
    };

    model.onAddedTap = function (eventData) {
        switch(parseInt(eventData.object.tapCounter)){
            case 0:
                model.added_filter = "yes";
                model.set("added_class","button-success fa");
                eventData.object.tapCounter = "1";
                eventData.object.text = String.fromCharCode(parseInt('f067',16));
                break;
            case 1:
                model.added_filter = "no";
                model.set("added_class","button-error fa");
                eventData.object.tapCounter = "2";
                eventData.object.text = String.fromCharCode(parseInt('f00d',16));
                break;
            case 2:
                model.added_filter="";
                model.set("added_class","button-secondary fa");
                eventData.object.tapCounter = "0";
                eventData.object.text = String.fromCharCode(parseInt('f0c9',16));
                break;
        }
        model.set("items", _refilterOnAdded());
    };

    model.onFilterTap = function(eventData){
        if(eventData.object.active === false) {
            model.type_sort = eventData.object.type;
        } else {
            model.type_sort = "";
        }
        model.set("items", _refilterOnType(model.type_sort));
    };

    model.onPullToRefreshInitiated = function (eventData) {
        reloadFromBackend();
        eventData.object.notifyPullToRefreshFinished();
    };

    model.on_add_or_delete = function(eventData){
        if(eventData.object.added === "yes"){
            alert("remove is not implemented");
        } else {
            addedSensorIds.push(eventData.object.sensorId);
            fetch(global.backendUrl + '/measurements/' + measurementId, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    'Authorization': 'Bearer ' + global.accessToken
                },
                body: JSON.stringify({
                    sensors: addedSensorIds
                })
            }).then(r => { return r.json(); }).then(function (r) {
                Toast.makeText("Sensor added to the measurement", "long").show();
                var index =_.findIndex(model.allItems, function(o){return o.id === eventData.object.sensorId;});
                model.allItems[index].added_status = String.fromCharCode(parseInt('f00d',16));
                model.allItems[index].color = "button-error";
                model.allItems[index].real_added = "yes";
            }).catch(function(error) {
                console.log(error);
            });
        }
    };

    function _refilter() {
        var items = model.allItems;
        var filter = model.filter;
        if (filter) {
            var regexp = new RegExp(filter, 'i');
            items = _.filter(items, function(i) {
                return i.name.match(regexp);
            });
        }
        return items;
    }

    function _refilterOnType(eventData){
        var items = model.allItems;
        var regexp = new RegExp(eventData, 'i');
        items = _.filter(items, function(i) {
            return i.real_type.match(regexp);
        });
        return items;
    }

    function _refilterOnAdded() {
        var items = model.allItems;
        var regexp = new RegExp(model.added_filter, 'i');
        items = _.filter(items, function(i) {
            return i.real_added.match(regexp);
        });
        return items;
    }

    function _sort(){
        var items = model.items;
        items = _.orderBy(items, ['name'], [model.sort_mode]);
        return items;
    }

    function reloadFromBackend(){
        model.allItems = [];
        fetch(global.backendUrl + '/sensors', {
            headers: {
                'Authorization': 'Bearer ' + global.accessToken
            }
        }).then(response => { return response.json(); }).then(function (r) {
            for(var i = 0; i < r.length; i++){
                var _type;
                var jsonstatus = r[i].type;
                switch(jsonstatus){
                    case "temperature":
                        _type = String.fromCharCode(parseInt('f2c8',16));
                        break;
                    case "humidity":
                        _type = String.fromCharCode(parseInt('f043',16));
                        break;
                    case "noise":
                        _type = String.fromCharCode(parseInt('f028',16));
                        break;
                    case "light":
                        _type = String.fromCharCode(parseInt('f0eb',16));
                        break;
                }
                model.allItems.push({id: r[i]._id, name: r[i].name, type: _type, real_type: r[i].type,
                    added_status: String.fromCharCode(parseInt('f067', 16)),
                    color: "button-success", real_added: "no"
                });
            }
        }).then(function () {
            fetch(global.backendUrl + '/measurements/' + mId, {
                headers: {
                    'Authorization': 'Bearer ' + global.accessToken
                }
            }).then(response => {return response.json();}).then(function (r) {
                for(var i = 0; i < r.sensors.length; i++){
                    var index =_.findIndex(model.allItems, function(o){return o.id == r.sensors[i];});
                    model.allItems[index].added_status = String.fromCharCode(parseInt('f00d',16));
                    model.allItems[index].color = "button-error";
                    model.allItems[index].real_added = "yes";
                    addedSensorIds.push(r.sensors[i]);
                }
            }).then(function(){
                model.set("items", _refilter());
            })
        }).catch(function(error) {
            console.log(error);
        });
    }

    model.on(Observable.propertyChangeEvent, function(e) {
        if (e.propertyName === "filter") model.set("items", _refilter());
    });

    return model;
};