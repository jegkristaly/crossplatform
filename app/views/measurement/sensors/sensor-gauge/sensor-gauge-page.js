var Observable = require("data/observable").Observable;
var gaugesModule = require("nativescript-pro-ui/gauges");
var sensorId;
var measurementId;
var needle;
var valueLabel;
var data = [];

exports.onNavigatingTo = function onNavigatingTo(args) {
    var page = args.object;
    var gotData=page.navigationContext;

    sensorId = gotData.sensorId;
    measurementId = gotData.measurementId;

    var gauge = page.getViewById("gaugeView");
    valueLabel = page.getViewById("valueLabel");

    needle = gauge.scales.getItem(0).indicators.getItem(0);
    gauge.scales.getItem(0).minimum = gotData.min;
    gauge.scales.getItem(0).maximum = gotData.max;

    page.bindingContext = new Observable();

    fetch(global.backendUrl + '/sensors/' + sensorId + '/data/' + measurementId, {
        headers: {
            'Authorization': 'Bearer ' + global.accessToken
        }
    }).then(response => { return response.json(); }).then(function (r) {
        for(var i = 0; i < r.data.length; i++){
            data.push(r.data[i].value);
        }
    }).then(function(){
        demo();
    }).catch(function(error) {
        console.log(error);
    });

};

exports.onNavBtnTap = function onNavBtnTap(eventData) {
    frameModule.topmost().goBack();
};

exports.onReplayTap = function(){
    demo();
};

async function demo(){
    for(var i = 0; i < data.length; i++){
        needle.value = data[i];
        valueLabel.text = data[i];
        await sleep(1000);
    }
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}