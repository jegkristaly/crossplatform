var Observable = require("data/observable").Observable;
var frameModule = require("ui/frame");
var nameTextField;
var minTextField;
var maxTextField;
var selectedTypeObjectId="typeTemperatureSwitch";
var selectedDistributionId="distributionRandomSwitch";

exports.onNavigatingTo = function onNavigatingTo(args) {
    var page = args.object;

    nameTextField = page.getViewById("nameTextField");
    minTextField = page.getViewById("minTextField");
    maxTextField = page.getViewById("maxTextField");

    page.bindingContext = new Observable();
};

exports.onNavBtnTap = function onNavBtnTap(eventData) {
    frameModule.topmost().goBack();
};

exports.onCreateTap = function onCreateTap(eventData) {
    var type;
    var distribution;
    switch(selectedTypeObjectId){
        case "typeTemperatureSwitch":
            type = "temperature";
            break;
        case "typeHumiditySwitch":
            type = "humidity";
            break;
        case "typeNoiseSwitch":
            type = "noise";
            break;
        case "typeLightSwitch":
            type = "light";
            break;
    }
    switch(selectedDistributionId){
        case "distributionRandomSwitch":
            distribution = "random";
            break;
        case "distributionLinearSwitch":
            distribution = "linear";
            break;
    }

    var navigationOptions={
        moduleName:'views/measurement/sensors/sensor-add/sensor-add-location/sensor-add-location-page',
        backstackVisible: false,
        context:{
            name: nameTextField.text,
            type: type,
            distribution: distribution,
            min: minTextField.text,
            max: maxTextField.text,
        }
    };
    frameModule.topmost().navigate(navigationOptions);
};

exports.typeLoaded = function(args){
    var obj = new Observable();
    obj.set("typeSelected", "typeTemperatureSwitch");
    obj.set("typeSwitchTap", function(args){
        setTimeout(function() {
            selectedTypeObjectId = args.object.id;
            obj.set("typeSelected", args.object.id);
        }, 100);
    });
    args.object.bindingContext = obj;
}

exports.distributionLoaded = function(args){
    var obj = new Observable();
    obj.set("distributionSelected", "distributionRandomSwitch");
    obj.set("distributionSwitchTap", function(args){
        setTimeout(function() {
            selectedDistributionId = args.object.id;
            obj.set("distributionSelected", args.object.id);
        }, 100);
    });
    args.object.bindingContext = obj;
}