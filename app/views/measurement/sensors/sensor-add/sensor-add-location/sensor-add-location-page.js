var Observable = require("data/observable").Observable;
var mapbox = require("nativescript-mapbox");
var frameModule = require("ui/frame");
var Toast = require("nativescript-toast");
var map;
var name;
var type;
var distribution;
var min;
var max;
var lastLat;
var lastLng;

exports.onNavigatingTo = function onNavigatingTo(args) {
    var page = args.object;
    var viewModel = new Observable();

    var gotData=page.navigationContext;
    name = gotData.name;
    type = gotData.type;
    distribution = gotData.distribution;
    min = gotData.min;
    max = gotData.max;

    page.bindingContext = viewModel;
};

exports.onMapReady = function onMapReady(args){
    args.map.setOnMapClickListener(function(point) {
        lastLat = point.lat;
        lastLng = point.lng;
        args.map.removeMarkers();
        args.map.addMarkers(
            [
                {
                    id: 1, // can be user in 'removeMarkers()'
                    lat: point.lat, // mandatory
                    lng: point.lng, // mandatory
                    title: 'Selected sensor location', // no popup unless set
                    icon: 'res://cool_marker', // preferred way, otherwise use:
                    //icon: 'http(s)://website/coolimage.png', // from the internet (see the note at the bottom of this readme), or:
                    //iconPath: 'res/markers/home_marker.png',
                }
            ]
        )
    });
};


exports.onNavBtnTap = function onNavBtnTap(eventData) {
    frameModule.topmost().goBack();
};

exports.onConfirmTap = function onConfirmTap(){
    fetch(global.backendUrl + '/sensors/', {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
            'Authorization': 'Bearer ' + global.accessToken
        },
        body: JSON.stringify({
            name: name,
            type: type,
            distribution: distribution,
            min: min,
            max: max,
            coordinates: {
                lat: lastLat,
                long: lastLng
            }
        })
    }).then(r => { return r.json(); }).then(function (r) {
        Toast.makeText("Sensor created", "long").show();
        var navigationOptions={
            moduleName:'views/localdemo/demo',
            backstackVisible: false
        };
        frameModule.topmost().navigate(navigationOptions);
    }).catch(function(error) {
        alert(error);
    });
};
