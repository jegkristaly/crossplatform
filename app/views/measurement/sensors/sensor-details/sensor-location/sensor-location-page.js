var Observable = require("data/observable").Observable;
var mapbox = require("nativescript-mapbox");
var frameModule = require("ui/frame");
var map;
var lat;
var long;
var name;
var type;
var distribution;

exports.onNavigatingTo = function onNavigatingTo(args) {
    var page = args.object;
    var viewModel = new Observable();
    var gotData=page.navigationContext;

    lat = gotData.lat;
    long = gotData.long;
    name = gotData.name;
    type = gotData.type;
    distribution = gotData.distribution;

    page.bindingContext = viewModel;
};

exports.onMapReady = function onMapReady(args){
    args.map.addMarkers(
        [
            {
                id: 1, // can be user in 'removeMarkers()'
                lat: lat, // mandatory
                lng: long, // mandatory
                title: name, // no popup unless set
                subtitle: 'Type: ' + type + '\nDistribution: ' + distribution,
                icon: 'res://cool_marker', // preferred way, otherwise use:
                //icon: 'http(s)://website/coolimage.png', // from the internet (see the note at the bottom of this readme), or:
                //iconPath: 'res/markers/home_marker.png',
            }
        ]
    )
};


exports.onNavBtnTap = function onNavBtnTap(eventData) {
    frameModule.topmost().goBack();
};

exports.onConfirmTap = function onConfirmTap(){
    frameModule.topmost().goBack();
};
