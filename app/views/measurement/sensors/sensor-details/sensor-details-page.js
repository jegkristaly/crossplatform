var Observable = require("data/observable").Observable;
var frameModule = require("ui/frame");
var sensorId;
var measurementId;
var min;
var max;
var gaugeButton;
var long;
var lat;
var name;
var type;
var distribution;

function onNavigatingTo(args) {
    var page = args.object;
    var gotData=page.navigationContext;
    var temp = "visible";

    sensorId = gotData.sensorId;
    measurementId = gotData.measurementId;

    gaugeButton = page.getViewById("gaugeButton");

    if(gotData.from !== "measurements"){
        temp="collapsed";
    }

    fetch(global.backendUrl + '/sensors/' + sensorId, {
        headers: {
            'Authorization': 'Bearer ' + global.accessToken
        }
    }).then(function(response){
        if(response.ok){
            return response.json();
        } else {
            throw new Exception("something went wrong");
        }
    }).then(function(r){
        page.bindingContext = ({
            name: r.name,
            type: r.type,
            distribution: r.distribution,
            max: r.max,
            min: r.min,
            visibility: temp,
        });
        lat = r.coordinates.lat;
        long = r.coordinates.long;
        min = r.min;
        max = r.max;
        name = r.name;
        type = r.type;
        distribution = r.distribution;
    }).catch(function(error) {
        console.log(error);
    });

    page.bindingContext = new Observable();
}

exports.onNavigatingTo = onNavigatingTo;

function onDeleteTap(eventData){
    alert("Not implemented");
}
exports.onDeleteTap = onDeleteTap;

function onNavBtnTap(eventData) {
    frameModule.topmost().goBack();
}
exports.onNavBtnTap = onNavBtnTap;

exports.onGaugeTap = function (){
    var navigationOptions={
        moduleName:'views/measurement/sensors/sensor-gauge/sensor-gauge-page',
        context:{
            sensorId: sensorId,
            measurementId: measurementId,
            min: min,
            max: max
        }
    };
    frameModule.topmost().navigate(navigationOptions);
};

exports.onLocationTap = function(){
    var navigationOptions={
        moduleName:'views/measurement/sensors/sensor-details/sensor-location/sensor-location-page',
        context:{
            lat: lat,
            long: long,
            name: name,
            type: type,
            distribution: distribution
        }
    };
    frameModule.topmost().navigate(navigationOptions);
};