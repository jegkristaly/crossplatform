var Observable = require("data/observable").Observable;
var frameModule = require("ui/frame");
var mapModel = require("./map-model-page")

exports.onNavigatingTo = function onNavigatingTo(args) {
    var page = args.object;
    var viewModel = new Observable();
    var gotData=page.navigationContext;
    page.bindingContext = mapModel(gotData.measurementId);
};

exports.onNavBtnTap = function onNavBtnTap(eventData) {
    frameModule.topmost().goBack();
};
