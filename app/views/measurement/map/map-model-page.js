var Observable = require("data/observable").Observable;
var _ = require("lodash");

module.exports = function(measurementId) {
    var model = new Observable();

    var allItems = [];
    var sensorIds = [];
    var map;

    function reloadFromBackend(){
        fetch(global.backendUrl + '/measurements/' + measurementId, {
            headers: {
                'Authorization': 'Bearer ' + global.accessToken
            }
        }).then(function(response){
            if(response.ok){
                return response.json();
            } else {
                throw new Exception("an error occured...");
            }
        }).then(function(json){
            for(var i = 0; i < json.sensors.length; i++)
                sensorIds.push(json.sensors[i]);
        }).then(function() {
            for(var i = 0; i < sensorIds.length; i++) {
                fetch(global.backendUrl + '/sensors/' + sensorIds[i], {
                    headers: {
                        'Authorization': 'Bearer ' + global.accessToken
                    }
                }).then(function (response) {
                    if(response.ok){
                        return response.json();
                    } else {
                        throw new Exception("an error occured...");
                    }
                }).then(function(json2){
                    allItems.push({name: json2.name, lat: json2.coordinates.lat, long: json2.coordinates.long, type: json2.type});
                }).then(function(){
                    for(var i = 0; i < allItems.length; i++) {
                        map.addMarkers(
                            [
                                {
                                    lat: allItems[i].lat, // mandatory
                                    lng: allItems[i].long, // mandatory
                                    title: allItems[i].name,
                                    subtitle: allItems[i].type,
                                    icon: 'res://cool_marker', // preferred way, otherwise use:
                                }
                            ]
                        )
                    }
                })
            }
        }).catch(function(error) {
            console.log(error);
        });
    }

    model.onMapReady = function(eventData){
        map = eventData.map;
        reloadFromBackend();
    };

    return model;
};