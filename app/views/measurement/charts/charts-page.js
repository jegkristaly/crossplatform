var frameModule = require("ui/frame");
var Observable = require("data/observable").Observable;
var chartsModel = require("./charts-view-model");


exports.pageLoaded = function(args) {
    var page = args.object;
    var gotData=page.navigationContext;
    page.bindingContext = chartsModel(gotData.measurementId);
};

exports.onNavBtnTap = function onNavBtnTap(eventData) {
    frameModule.topmost().goBack();
};
