var Observable = require("data/observable").Observable;
var ObservableArray = require("data/observable-array").ObservableArray;

module.exports = function(measurementId) {
    var model = new Observable();

    var sensorsIds = [];
    model.Data = new ObservableArray();
    reloadFromBackend();

    function reloadFromBackend(){
        fetch(global.backendUrl + '/measurements/' + measurementId, {
            headers: {
                'Authorization': 'Bearer ' + global.accessToken
            }
        }).then(function(response){
            if(response.ok){
                return response.json();
            } else {
                throw new Exception("an error occured...");
            }
        }).then(function(json){
            for(var i = 0; i < json.sensors.length; i++)
                sensorsIds.push(json.sensors[i]);
        }).then(function() {
            for(var i = 0; i < sensorsIds.length; i++) {
                fetch(global.backendUrl + '/sensors/' + sensorsIds[i] + '/data/' + measurementId, {
                    headers: {
                        'Authorization': 'Bearer ' + global.accessToken
                    }
                }).then(function(response){
                    if(response.ok){
                        return response.json();
                    } else {
                        throw new Exception("something went wrong");
                    }
                }).then(function(json2){
                    for(var j = 0; j < json2.data.length; j++){
                        model.Data.push(
                            {
                                season: j,
                                count: json2.data[j].value
                            }
                        )
                    }
                });
            }
        }).catch(function(error) {
            console.log(error);
        });
    }
    return model;
};
