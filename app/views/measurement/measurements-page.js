var MeasurementModel = require("./measurements-view-model");
var frameModule = require("ui/frame");

exports.onNavigatingTo = function(e) {
    var page = e.object;
    var gotData=page.navigationContext;
    creator = gotData.creator;
    page.bindingContext = new MeasurementModel();
};

exports.sbLoaded = function sbLoaded(eventData){
    eventData.object.android.clearFocus();
};

exports.onChildTap = function onChildTap(eventData) {
    var navigationOptions={
        moduleName:'views/measurement/measurement/measurement-page',
        context:{
            measurementId: eventData.object.recordId,
            name: eventData.object.name,
            status: eventData.object.status
        }
    };
    frameModule.topmost().navigate(navigationOptions);
};

exports.onNewMeasurementTap = function onNewMeasurementTap(eventData) {
    var navigationOptions={
        moduleName:'views/measurement/add-new/add-new-page',
        context:{creator: creator
        }
    };
    frameModule.topmost().navigate(navigationOptions);
};

exports.onNavBtnTap = function onNavBtnTap(eventData) {
    frameModule.topmost().goBack();
};