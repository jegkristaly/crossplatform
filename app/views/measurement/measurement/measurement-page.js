var frameModule = require("ui/frame");
var Observable = require("data/observable").Observable;
var measurementId;
var Toast = require("nativescript-toast");

exports.onNavigatingTo = function onNavigatingTo(args) {
    var page = args.object;

    var myLabel = page.getViewById("measurementActionBar");
    var gotData=page.navigationContext;

    myLabel.title= gotData.name;
    measurementId = gotData.measurementId;

    page.bindingContext = new Observable();
};


exports.onDetailsTap = function onDetailsTap(eventData) {
    var navigationOptions={
        moduleName:'views/measurement/details/details-page',
        context:{measurementId: measurementId
        }
    };
    frameModule.topmost().navigate(navigationOptions);
};


exports.onSensorsTap = function onSensorsTap(eventData) {
    var navigationOptions={
        moduleName:'views/measurement/sensors/sensors-page',
        context:{measurementId: measurementId
        }
    };
    frameModule.topmost().navigate(navigationOptions);
};

exports.onGraphsTap = function onGraphsTap(eventData) {
    var navigationOptions={
        moduleName:'views/measurement/charts/charts-page',
        context:{measurementId: measurementId
        }
    };
    frameModule.topmost().navigate(navigationOptions);
};


exports.onFinishTap = function onFinishTap(eventData) {
    fetch(global.backendUrl + '/measurements/' + measurementId, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json",
            'Authorization': 'Bearer ' + global.accessToken
        },
        body: JSON.stringify({
            live: false
        })
    }).then(r => { return r.json(); }).then(function (r) {
        Toast.makeText("Measurement stopped", "long").show();
    }).catch(function(error) {
        console.log(error);
    });
};

exports.onNavBtnTap = function onNavBtnTap(eventData) {
    frameModule.topmost().goBack();
};

exports.onMapTap = function(){
    var navigationOptions={
        moduleName:'views/measurement/map/map-page',
        context:{measurementId: measurementId
        }
    };
    frameModule.topmost().navigate(navigationOptions);
};