var frameModule = require("ui/frame");
let observableArray = require("data/observable-array");
var creator;

exports.onNavigatingTo = function onNavigatingTo(args) {
    var page = args.object;

    var gotData=page.navigationContext;
    creator = gotData.creator;

    var array = new observableArray.ObservableArray();

    fetch(global.backendUrl + '/sensors', {
        headers: {
            'Authorization': 'Bearer ' + global.accessToken
        }
    }).then(response => { return response.json(); }).then(function (r) {
        for(var i = 0; i < r.length; i++){
            array.push({id: r[i]._id, name: r[i].name, type_and_distribution: r[i].type + " - " + r[i].distribution });
        }
        page.bindingContext = {items: array};
    }).catch(function(error) {
        console.log(error);
    });
};

exports.onNavBtnTap = function onNavBtnTap(eventData) {
    frameModule.topmost().goBack();
};

exports.onNotImplemented = function onNotImplemented(eventData){
    alert("Not implemented!");
};