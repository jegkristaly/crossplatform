var Observable = require("data/observable").Observable;
var frameModule = require("ui/frame");
var emailTextField;
var passwordTextField;
var Toast = require("nativescript-toast");

exports.onNavigatingTo = function onNavigatingTo(args) {
    var page = args.object;
    var viewModel = new Observable();
    emailTextField = page.getViewById("emailTextField");
    passwordTextField = page.getViewById("passwordTextField");
    page.bindingContext = viewModel;
};

exports.onNavBtnTap = function onNavBtnTap(eventData) {
    frameModule.topmost().goBack();
};

exports.onRegisterTap = function onRegisterTap(){
    if(emailTextField.text && passwordTextField.text) {
        fetch(global.backendUrl + '/register', {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
                username: emailTextField.text,
                password: passwordTextField.text
            })
        }).then(function (response) {
            if(response.ok){
                Toast.makeText("Registered", "long").show();
                frameModule.topmost().goBack();
            } else {
                response.json().then(json => {Toast.makeText(json.message, "long").show();});
                frameModule.topmost().goBack();
            }
        }).catch(function(error) {
            //nem csinálunk semmit
        });
    } else {
        Toast.makeText("All Fields Required!", "long").show();
    }
};