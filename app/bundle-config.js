if (global.TNS_WEBPACK) {
    //registers tns-core-modules UI framework modules
    require("bundle-entry-points");

    //register application modules
    global.registerModule("views/home/home-page", function () { return require("./views/home/home-page"); });
}
