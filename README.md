# Jégkristály csapat szenzoros projektjének crossplaftorm alkalmazása

## Telepítés

Mindenkori legfrissebb commithoz tartozó (aláíratlan) apk elérhető a repo-ból, vagy közvetlen ezen a [linken](https://gitlab.com/jegkristaly/crossplatform/-/jobs/artifacts/master/download?job=build).

[QR - kód](https://api.qrserver.com/v1/create-qr-code/?size=400x400&data=https://gitlab.com/jegkristaly/crossplatform/-/jobs/artifacts/master/download?job=build)